#include <stdio.h>
#include <stdlib.h>
#include "IntList.h"

struct IntNode * newIntNode(int v, struct IntNode * n) {
  struct IntNode * node;
  node = malloc(sizeof(*node));
  (*node).value = v;
  (*node).next = n;
  return node;
}

struct IntList * newIntList() {
  struct IntList * list;
  list = malloc(sizeof(*list));
  (*list).head = NULL;
  return list;
}

void deleteIntList(struct IntList * list) {
  if(isEmpty(list)){
	free(list);
} 
struct IntNode * first;
   first = getFront(list);
struct IntNode * temp;
if(first != NULL){
 do{
    temp = first;
    first = (*first).next;
    free(temp);
  }while(first != NULL);
free(list);
} 
}

void insertFront(struct IntList * list, int v) {
  (*list).head = newIntNode(v, (*list).head);
}

void removeFront(struct IntList * list) {
  if (!isEmpty(list)) {
    (*list).head = (*((*list).head)).next;
  }
}

bool isEmpty(struct IntList * list) {
  return (*list).head == NULL;
}

int size(struct IntList * list) {
  int size = 0;
   struct IntNode * first;
   first = getFront(list);
  if (first != NULL) {
     do {
	size++;
	first = (*first).next;	
    } while (first != NULL);
  }
  return size;
}

struct IntNode * getFront(struct IntList * list) {
  return (*list).head;
}

struct IntList * getRest(struct IntList * list) {
  if (isEmpty(list)) {
    return NULL;
  }
  struct IntList * rest;
  rest = newIntList();
  (*rest).head = (*((*list).head)).next;
  return rest;
}

void printIntList(struct IntList * list) {
  printf("[ ");
  if (!isEmpty(list)) {
     struct IntNode * first;
     first = getFront(list);
    do {
      
      printf("%d ",(*first).value);
      first = (*first).next;
    } while (first != NULL);
  }
  printf("]");
}

