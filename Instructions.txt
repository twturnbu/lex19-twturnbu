
FOR LEX 19 I ADDED MY PROCESS TO THE BOTTOM OF THIS TEXT FILE, 
EVERYTHING ELSE IS UNTOUCHED FROM LEX18

LEX 18 - Thursday April 12

OVERVIEW
--------
In this lab exercise you will be working with the memcheck tool from
the valgrind suite.  In class yesterday we explored several errors
that memcheck can detect, and discussed what code problems can give
rise to them.  In this exercise you get to explore memory problems in
some code, with the goal being to resolve them.

The exercise is split into two parts, partA and partB.  The code for
each part is its own directory.  Start with partA and then move on to
partB.

Apply good development and debugging practices, including use of a
repo in BitBucket and branching as appropriate.  When you run
valgrind, use either of these forms:

	  valgrind --tool=memcheck --leak-check=yes -v ./main 
	  valgrind --tool=memcheck --leak-check=yes ./main 

You may incorporate these into targets in the makefiles to make
running the code easier.


PART A
------
 
The first thing to do is to compile and run the code:

    make main
    ./main

What output is produced?

RESPONSE 1:

Inserting 0 into [ ]
Inserting 1 into [ 0 ]
Inserting 2 into [ 1 0 ]
Inserting 3 into [ 2 1 0 ]
Inserting 4 into [ 3 2 1 0 ]
Inserting 5 into [ 4 3 2 1 0 ]
Inserting 6 into [ 5 4 3 2 1 0 ]
Inserting 7 into [ 6 5 4 3 2 1 0 ]
Inserting 8 into [ 7 6 5 4 3 2 1 0 ]
Inserting 9 into [ 8 7 6 5 4 3 2 1 0 ]
[ 9 8 7 6 5 4 3 2 1 0 ] has size 10


Run the exectuable using valgrind:

    valgrind --tool=memcheck --leak-check=yes ./main 

Study the section of the output dealing with memory leaks.  You should
see a table like that looks like this:

==31698== LEAK SUMMARY:
==31698==    definitely lost: 0 bytes in 0 blocks
==31698==    indirectly lost: 0 bytes in 0 blocks
==31698==      possibly lost: 0 bytes in 0 blocks
==31698==    still reachable: 0 bytes in 0 blocks
==31698==         suppressed: 0 bytes in 0 blocks

Your table should have some non-zero byte and block counts.  Show your
table here:

RESPONSE 2:

==22255== LEAK SUMMARY:
==22255==    definitely lost: 528 bytes in 66 blocks
==22255==    indirectly lost: 160 bytes in 10 blocks
==22255==      possibly lost: 0 bytes in 0 blocks
==22255==    still reachable: 0 bytes in 0 blocks
==22255==         suppressed: 0 bytes in 0 blocks


Explain, in your own words, what the non-zerp byte and block counts
refer to.  You may consult the valgrind documentation
(e.g. http://valgrind.org/docs/manual/faq.html#faq.deflost).  Do not
simply copy text from the documentation - rephrase using your own
words.

RESPONSE 3:
definitely lost refers to memory that you leaked directly, while indirectly leaked are 
cases where pointers are causing leaks 

You will notice in the IntList.c file that the function deleteIntList
has an empty definition.  Define this function so that it deletes the
IntList it points to (including the IntNodes that it contains).

After you define this function, check the LEAK SUMMARY again
(reproducing the results below):

RESPONSE 4:
==27426== LEAK SUMMARY:
==27426==    definitely lost: 528 bytes in 66 blocks
==27426==    indirectly lost: 0 bytes in 0 blocks
==27426==      possibly lost: 0 bytes in 0 blocks
==27426==    still reachable: 0 bytes in 0 blocks
==27426==         suppressed: 0 bytes in 0 blocks



You may find memory is still being leaked.  Do your best to get down
to zero memory leaks.  If you have not eliminated the leaks by the
midpoint of the lab, give your last LEAK SUMMARY report, and continue
with Part B.


PART B
------
  
The first thing to do is to compile and run the code:

    make main
    ./main

What output is produced?

RESPONSE 5:
"Buffy" = "Buffy"
"Xander" = "Xander"
"Willow" = "Willow"
"Cordelia" = "Cordelia"
"Rupert" = "Rupert"
"Oz" = "Oz"
"Spike" = "Spike"
"Rupert" = "Rupert"
"Dawn" = "Dawn"
"Tara" = "Tara"
"Angel" = "Angel"
Inserting "Buffy" into [ ]
Inserting "Xander" into [ "Buffy" ]
Inserting "Willow" into [ "Xander" "Buffy" ]
Inserting "Cordelia" into [ "Willow" "Xander" "Buffy" ]
Inserting "Rupert" into [ "Cordelia" "Willow" "Xander" "Buffy" ]
Inserting "Oz" into [ "Rupert" "Cordelia" "Willow" "Xander" "Buffy" ]
Inserting "Spike" into [ "Oz" "Rupert" "Cordelia" "Willow" "Xander" "Buffy" ]
Inserting "Rupert" into [ "Spike" "Oz" "Rupert" "Cordelia" "Willow" "Xander" "Buffy" ]
Inserting "Dawn" into [ "Rupert" "Spike" "Oz" "Rupert" "Cordelia" "Willow" "Xander" "Buffy" ]
Inserting "Tara" into [ "Dawn" "Rupert" "Spike" "Oz" "Rupert" "Cordelia" "Willow" "Xander" "Buffy" ]
Inserting "Angel" into [ "Tara" "Dawn" "Rupert" "Spike" "Oz" "Rupert" "Cordelia" "Willow" "Xander" "Buffy" ]
Inserting "Buffy" into [ ]
Inserting "Willow" into [ "Buffy" ]
Inserting "Rupert" into [ "Willow" "Buffy" ]
Inserting "Spike" into [ "Rupert" "Willow" "Buffy" ]
Inserting "Dawn" into [ "Spike" "Rupert" "Willow" "Buffy" ]
Inserting "Angel" into [ "Dawn" "Spike" "Rupert" "Willow" "Buffy" ]
Inserting "Xander" into [ ]
Inserting "Cordelia" into [ "Xander" ]
Inserting "Oz" into [ "Cordelia" "Xander" ]
Inserting "Rupert" into [ "Oz" "Cordelia" "Xander" ]
Inserting "Tara" into [ "Rupert" "Oz" "Cordelia" "Xander" ]
Before "Spike" to "SpIke" and other edits
[ "Angel" "Tara" "Dawn" "Rupert" "Spike" "Oz" "Rupert" "Cordelia" "Willow" "Xander" "Buffy" ] has size 11
[ "Angel" "Dawn" "Spike" "Rupert" "Willow" "Buffy" ] has size 6
[ "Tara" "Rupert" "Oz" "Cordelia" "Xander" ] has size 5
After "Spike" to "SpIke" and other edits
[ "Angel" "Dawn" "SpIke" "Rupert" "Willow" "Buffy" ] has size 6
[ "Tara" "Rupert" "AU" "Cordelia" "Xander" ] has size 5
[ "Angel" "Tara" "Dawn" "Rupert" "SpIke" "AU" "Rupert" "Cordelia" "Willow" "Xander" "Buffy" ] has size 11


Run the exectuable using valgrind:

    valgrind --tool=memcheck --leak-check=yes ./main 

Study the section of the output dealing with memory leaks.  You should
see a table like that looks like this:

==31698== LEAK SUMMARY:
==31698==    definitely lost: 0 bytes in 0 blocks
==31698==    indirectly lost: 0 bytes in 0 blocks
==31698==      possibly lost: 0 bytes in 0 blocks
==31698==    still reachable: 0 bytes in 0 blocks
==31698==         suppressed: 0 bytes in 0 blocks

Your table should have some non-zero byte and block counts.  Show your
table here:

RESPONSE 6:

==27787== LEAK SUMMARY:
==27787==    definitely lost: 1,368 bytes in 171 blocks
==27787==    indirectly lost: 420 bytes in 33 blocks
==27787==      possibly lost: 0 bytes in 0 blocks
==27787==    still reachable: 0 bytes in 0 blocks
==27787==         suppressed: 0 bytes in 0 blocks


In this part you will notice, in the StringList.c file, that the
function deleteStringList has a definition.  However, it may not be
correct and complete.  Don't change it for now (that comes later), but
for now notice that in main.c there are three commented out calls to
this function, as in:

  /* DELETE #1 */
  //  deleteStringList(evenStrings);

Comment in the first call, labelled with /* DELETE #1 */, then
recompile and rerun memcheck:

    make clean
    make main
    valgrind --tool=memcheck --leak-check=yes ./main 

Show your LEAK SUMMARY table here:

RESPONSE 7:
    
==28045== LEAK SUMMARY:
==28045==    definitely lost: 1,368 bytes in 171 blocks
==28045==    indirectly lost: 324 bytes in 27 blocks
==28045==      possibly lost: 0 bytes in 0 blocks
==28045==    still reachable: 0 bytes in 0 blocks
==28045==         suppressed: 0 bytes in 0 blocks

Do the same with the call labelled with /* DELETE #2 */.  Show your LEAK SUMMARY table here:

RESPONSE 8:

==28242==    definitely lost: 1,368 bytes in 171 blocks
==28242==    indirectly lost: 244 bytes in 22 blocks
==28242==      possibly lost: 0 bytes in 0 blocks
==28242==    still reachable: 0 bytes in 0 blocks
==28242==         suppressed: 0 bytes in 0 blocks    

Do the same with the call labelled with /* DELETE #3 */.  Show your LEAK SUMMARY table here:

RESPONSE 9:
    
==28357==    definitely lost: 1,436 bytes in 182 blocks
==28357==    indirectly lost: 0 bytes in 0 blocks
==28357==      possibly lost: 0 bytes in 0 blocks
==28357==    still reachable: 0 bytes in 0 blocks
==28357==         suppressed: 0 bytes in 0 blocks


deleteStringList does not delete the StringList struct that getRest
creates.  Modify deleteStringList so it also deletes this structure.
Give your revised definition here:

RESPONSE 10:
    void deleteStringList(struct StringList * list) {
  if (isEmpty(list)) {
     free(list);
  }
  else {
    do {
      struct StringNode * first;
      struct StringList * oldList;
      oldList = list;
      first = getFront(oldList);
      list = getRest(oldList);
      (*first).value = NULL;
      (*first).next = NULL;
      free(first);
      free(oldList);
    } while (!isEmpty(list));
    free(list);
  }
}


Recompile, show your LEAK SUMMARY table here:

RESPONSE 11:
    
==28794== LEAK SUMMARY:
==28794==    definitely lost: 1,412 bytes in 179 blocks
==28794==    indirectly lost: 0 bytes in 0 blocks
==28794==      possibly lost: 0 bytes in 0 blocks
==28794==    still reachable: 0 bytes in 0 blocks
==28794==         suppressed: 0 bytes in 0 blocks


OK - now let's think about another aspect to what this function could
do: should deleteStringList also delete the data (the strings) the
list holds?  Notice that this is a little problematic as allStrings
shares contents with boththe evenStrings list and the oddStrings list.

Define deleteStringList so it also deletes the strings contained in
the list, not just the list structure.  Recompile, show your LEAK
SUMMARY table here:

RESPONSE 12:
    
==30872== LEAK SUMMARY:
==30872==    definitely lost: 1,344 bytes in 168 blocks
==30872==    indirectly lost: 0 bytes in 0 blocks
==30872==      possibly lost: 0 bytes in 0 blocks
==30872==    still reachable: 0 bytes in 0 blocks
==30872==         suppressed: 0 bytes in 0 blocks

Does this change cause other problems in the code (e.g. unintended
output, or read/write errors reported by valgrind?)


RESPONSE 13:

According to valgrind there are some invalid reads, 
and the last line of output is garbage characters.

You may find memory is still being leaked.  Do your best to get down
to zero memory leaks.  If you have not eliminated the leaks by the
endpoint of the lab, give your last LEAK SUMMARY report, zip and
submit.   Also be sure to commit and push to your Bitbucket repo.

RESPONSE 14:

==3042== LEAK SUMMARY:
==3042==    definitely lost: 1,344 bytes in 168 blocks
==3042==    indirectly lost: 0 bytes in 0 blocks
==3042==      possibly lost: 0 bytes in 0 blocks
==3042==    still reachable: 0 bytes in 0 blocks
==3042==         suppressed: 0 bytes in 0 blocks
==3042== 

Kept trying to figure out other ways to reduce memory leaks but I just can not reduce it further. 
I think the strings array is leaking memory somewhere but can not determine how to fix it...

That's it - well done!

------------------------------------------------------------------------
END
------------------------------------------------------------------------

SO.. LEX 19 begins here!

I decided to start working on partB first cause why not. 
From trying to analyze the cryptic nonsense of memcheck I found that newStringList and 
getRest pop up a lot. So I started tracking down where these functions are being used. 

The first place I found getRest being used was printStringList. It assigned the returned
value into list, so once print string list is finished I decided to free it. 

My leak report before the changes: 

==21468== LEAK SUMMARY:
==21468==    definitely lost: 1,344 bytes in 168 blocks
==21468==    indirectly lost: 0 bytes in 0 blocks
==21468==      possibly lost: 0 bytes in 0 blocks
==21468==    still reachable: 0 bytes in 0 blocks
==21468==         suppressed: 0 bytes in 0 blocks


Leak summary after change: 

==23690== LEAK SUMMARY:
==23690==    definitely lost: 1,144 bytes in 143 blocks
==23690==    indirectly lost: 0 bytes in 0 blocks
==23690==      possibly lost: 0 bytes in 0 blocks
==23690==    still reachable: 0 bytes in 0 blocks
==23690==         suppressed: 0 bytes in 0 blocks

So 200 bytes was saved with that change. 

Next I found that the size function was using getRest, so I freed the list there.
Well now that I looked at the output of the program I realize that now the prints are all
messed up so my changes seem to be problematic..

HOLY COW, just saved 992 bytes!!!!
HOW? Let me tell you,
So I did some good ole googlin and found that if you 
add -ggdb3 to the CFLAGS variable in the makefile 
when you run the memcheck you get a more direct and less cryptic output.

Example: 

==28529== 88 bytes in 11 blocks are definitely lost in loss record 6 of 6
==28529==    at 0x4A0728A: malloc (vg_replace_malloc.c:299)
==28529==    by 0x4006A2: newStringList (StringList.c:15)
==28529==    by 0x40087A: getRest (StringList.c:76)
==28529==    by 0x400820: size (StringList.c:60)
==28529==    by 0x400DA3: main (main.c:78)

As opposed to before where it showed the path to main. This made it a little easier to track things down. 

So then in the printStringList function I changed the loop to keep using first, rather 
than using getRest with list every time. 

void printStringList(struct StringList * list) {
  printf("[ ");
  if (!isEmpty(list)) {
      struct StringNode * first;
      first = getFront(list);
    do {

      printf("\"%s\" ",(*first).value);
      first = (*first).next;
    } while (first != NULL);
  }
  printf("]");
}

So now my leak report is...

==28529== LEAK SUMMARY:
==28529==    definitely lost: 352 bytes in 44 blocks
==28529==    indirectly lost: 0 bytes in 0 blocks
==28529==      possibly lost: 0 bytes in 0 blocks
==28529==    still reachable: 0 bytes in 0 blocks
==28529==         suppressed: 0 bytes in 0 blocks

WOOOOOOO
I think I did it... 

So I found that the size function was also doing something similar with list and getRest. 
So I did a similar fix and just iterated through the list rather than using getRest.

CODE: 

int size(struct StringList * list) {
  int size = 0;
  struct StringNode * first;
  first = getFront(list);
  if (first != NULL) {
     do {
        size++;
         first = (*first).next;
     }
     while (first != NULL);
  }
  return size;
}


ANDDDD Heap Summary is now: 
==29839== HEAP SUMMARY:
==29839==     in use at exit: 0 bytes in 0 blocks
==29839==   total heap usage: 58 allocs, 69 frees, 620 bytes allocated
==29839== 
==29839== All heap blocks were freed -- no leaks are possible


So now I went back to part A to see if I can figure it out.. 
I changed the makefile to use -ggdb3 and I tracked down the first leak 
were printList was using getRest andthat was causing huge leaks. I changed it 
in the same way as i did in partB by just using the first...

void printIntList(struct IntList * list) {
  printf("[ ");
  if (!isEmpty(list)) {
     struct IntNode * first;
     first = getFront(list);
    do {

      printf("%d ",(*first).value);
      first = (*first).next;
    } while (first != NULL);
  }
  printf("]");
}



ALRIGHT! I got partA down to no leaks. First I changed the size function just like in partB, 
and then I modified the delete function a bit because I still had 8 bytes left over. 

So no my size function is: 

int size(struct IntList * list) {
  int size = 0;
   struct IntNode * first;
   first = getFront(list);
  if (first != NULL) {
     do {
        size++;
        first = (*first).next;
    } while (first != NULL);
  }
  return size;
}


and now my heap summary is:

==1888== HEAP SUMMARY:
==1888==     in use at exit: 0 bytes in 0 blocks
==1888==   total heap usage: 11 allocs, 11 frees, 168 bytes allocated
==1888== 
==1888== All heap blocks were freed -- no leaks are possible


